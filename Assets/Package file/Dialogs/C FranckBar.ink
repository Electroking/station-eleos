Franck : Bonjour et bienvenu dans mon auberge, en quoi puis-je vous aider ? 
*[suivant]
Franck : Le Doc ? C’est un médecin très réputé, qu’on les aime ou non, on ne peut pas nier qu’il est l’androïde le mieux conçu de la station ! 
    **[suivant]
    Franck : Il a sauvé de nombreuses vies, et il en sauvera certainement encore d’autres. 
        ***[suivant]
        Franck : Je sais qu’il a un assistant, un jeune homme assez réservé, je ne le connais pas personnellement. 
            ****[suivant]
            Franck : En effet, voir un humain sous les ordres d’un androïde, c’est vraiment pas commun ! 
                *****[suivant]
                Franck : C’est un grand ami de Mrs Cunningham, ils se connaissent depuis des années, allez la voir si vous le souhaitez, elle est très à l’écoute de la population et très disponible. 
                    ******[suivant]
                    Franck : Je vous sers quelque chose ?
                        *******[suivant]
                        Franck :Très bien, si l’envie vous prend, joignez-vous à nous ! Et mes salutations au Doc !
                        ->END