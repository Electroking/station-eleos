Mrs. Cunningham: Bonjour Inspecteur, je suis soulagé de voir savoir ici. J’aurais aimé vous accueillir sur la station dans de meilleures circonstances.
*[suivant]
Mrs. Cunningham: Je suis dévastée.  
    **[suivant]
    Mrs. Cunningham: Le Docteur Jekyll était l’élément le plus important de la station. C’était un grand ami d’enfance, je le connaissais depuis tant d’années !
        ***[suivant]
        Mrs. Cunningham: Sans lui la vie de beaucoup d’habitants de la station est en péril. Il était le seul capable de soigner certaines maladies.
            ****[suivant]
            Mrs. Cunningham: Beaucoup de gens risquent de disparaître si nous ne trouvons pas de solutions.
                *****[suivant]
                Mrs. Cunningham: Y compris moi. Je suis malade, et lui seul connaissait le traitement qu’il me fallait. Si je disparais, quelqu’un devra me remplacer. Il y a beaucoup de gens malhonnêtes qui veulent ma peau, celle des androïdes, et ma place.
                    ******[suivant]
                    Mrs. Cunningham: Mon bureau est à votre disposition, cherchez où vous le souhaitez, je reste à proximité.