Ridley : Laissez-moi sortir d’ici, le Docteur Jekyll est hors serv…mort, mais il faut que je continue son travail. 
*[suivant]
Ridley : Je ne suis pas menacé, je n’ai pas besoin de votre aide, je dois prendre la succession du Docteur. 
    **[suivant]
    Policier : Assistant, vous n’êtes plus sous protection policière, vous êtes suspecté du meurtre du Docteur Jekyll. Nous vous assignons donc à résidence et allons procéder à une fouille de vos quartiers.
        ***[suivant]
        Ridley: Moi ?! Un meurtrier ?! J’appréciais travailler avec le Doc, je n’aurais jamais fait une chose pareille !
            ****[suivant]
            Ridley: De toute façon, vous n’y trouverez rien d’intéressant. Vous perdez votre temps et me faites perdre le mien !
                *****[suivant]
                Ridley: Sans le Doc, je risque d’être un petit peu perdu, mais je dois garder la tête haute. J’avais encore beaucoup à apprendre. 
                    ******[suivant]
                    Inspecteur Sullyvan (vous) : Eh bien, on peut dire que Mr Ridley a l’air touché par la disparition du docteur. Il semble dévoué à poursuivre son travail. 
                        *******[suivant]
                        Inspecteur Sullyvan (vous) : Je vais procéder à la fouille de la chambre. Peut-être que mes doutes se confirmeront.
-->END