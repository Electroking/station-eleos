﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    public Image image;
    public float time = 0f, maxTime = 1f, finalTime = 0f;
    public bool fadingBlack = false, fadingWhite = false, goBlack = false, goWhite;

    public void Start()
    {
        image.gameObject.SetActive(true);
    }
    public void Update()
    {
        if(goBlack) //black
        {
            image.gameObject.SetActive(true);
            if (image != null)
            {
                fadingWhite = false;
                Debug.Log("fadingBlack");
                time = 0f;
                finalTime = 0f;
                fadingBlack = true;
                goBlack = false;
            }
            else
            {
                Debug.LogError("Image est null et ne peut donc pas Lerp");
            }
        }
        else if(goWhite) //white
        {
            if (image != null)
            {
                fadingBlack = false;
                Debug.Log("fadingWhite");
                time = 0f;
                finalTime = 0f;
                fadingWhite = true;
                goWhite = false;
            }
            else
            {
                Debug.LogError("Image est null et ne peut donc pas Lerp");
            }
        }

        if (fadingBlack)
        {
            if (image.color.a < maxTime)
            {
                image.color = Color.Lerp(image.color, new Color(image.color.r, image.color.g, image.color.b, 1f), finalTime);
                Debug.Log(image.color.a);
                time += Time.deltaTime;
                finalTime = time  / maxTime;
            }
            else if (image.color.a == maxTime)
            {
                fadingBlack = false;
            }
        }

        if (fadingWhite)
        {
            if (image.color.a < maxTime)
            {
                image.color = Color.Lerp(image.color, new Color(image.color.r, image.color.g, image.color.b, 0f), finalTime);
                //Debug.Log(image.color.a);
                time += Time.deltaTime;
                finalTime = time / maxTime;
            }
            else if (image.color.a == maxTime)
            {
                fadingWhite = false;
                image.gameObject.SetActive(false);
            }
        }
    }
}
