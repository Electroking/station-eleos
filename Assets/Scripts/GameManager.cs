﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public GameObject[] itemsImg;
    public GameObject panelInventory;

    public Button inventoryButton;

    #region panels
    public GameObject panelDocOffPart2;
    public GameObject panelDocOffPart3;
    
    public GameObject panelDocRoomPart1;
    public GameObject panelDocRoomPart2;

    public GameObject panelMayorOfficePart2;
    public GameObject panelMayorOfficePart3;

    public GameObject panelMayorRoomPart1;
    public GameObject panelMayorRoomPart2;

    public GameObject panelRidley1;
    public GameObject panelRidley2;
    

    #endregion

    public int itemsTokenDocOffice = 0;
    public int itemsTokenDocRoom = 0;
    public int itemsTokenMayorOffice = 0;
    public int itemsTokenMayorRoom = 0;
    public int itemsTokenRidleyRoom = 0;
    private void Awake()
    {
        instance = this;
        if (instance == null)
        {
            Destroy(gameObject);
            var go = Instantiate<GameObject>(new GameObject("GameManager"));
            go.AddComponent<GameManager>();
        }

        DontDestroyOnLoad(gameObject);
    }

    public void AddItem()
    {
        itemsTokenDocOffice++;
    }

    public void AddItemDocRoom()
    {
        itemsTokenDocRoom++;
    }
    
    public void AddItemMayorOffice()
    {
        itemsTokenMayorOffice++;
    }

    public void AddItemMayorRoom()
    {
        itemsTokenMayorRoom++;
    }
    public void AddItemRidleyRoom()
    {
        itemsTokenRidleyRoom++;
    }

    void Update()
    {
        if (itemsTokenDocOffice == 2)
        {
            panelDocOffPart2.SetActive(false);
            inventoryButton.enabled = true;
            panelDocOffPart3.SetActive(true);
            itemsTokenDocOffice = 0;
        }

        if (itemsTokenDocRoom == 2)
        {
            panelDocRoomPart1.SetActive(false);
            panelDocRoomPart2.SetActive(true);
            inventoryButton.enabled = true;
            itemsTokenDocOffice = 0;
        }

        if (itemsTokenMayorOffice == 2)
        {
            panelMayorOfficePart2.SetActive(false);
            panelMayorOfficePart3.SetActive(true);
            inventoryButton.enabled = true;

            itemsTokenMayorOffice = 0;
        }

        if (itemsTokenMayorRoom == 3)
        {
            panelMayorRoomPart1.SetActive(false);
            panelMayorRoomPart2.SetActive(true);
            inventoryButton.enabled = true;

            itemsTokenMayorRoom = 0;
        }

        if (itemsTokenRidleyRoom == 3)
        {
            panelRidley1.SetActive(false);
            panelRidley2.SetActive(true);
            inventoryButton.enabled = true;
            itemsTokenRidleyRoom = 0;
        }
    }
}
