﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    /*public GameObject panelInventory;
    public Image[] itemsImg;
    */

    public GameObject rapportO86;

    // Start is called before the first frame update
    void Start() 
    {
        GameManager.instance.panelInventory.SetActive(false);
        for (int i = 0; i < GameManager.instance.itemsImg.Length; i++)
        {
            GameManager.instance.itemsImg[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
