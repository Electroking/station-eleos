﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PointNClick : MonoBehaviour
{
    public static PointNClick instance;
    public List<GameObject> listPanel = new List<GameObject>();
    public int panelIndex = 0;

    public void ShowPanel()
    {
        listPanel[panelIndex].SetActive(true);
    }
    public void HidePanel()
    {
        listPanel[panelIndex].SetActive(false);
    }
}
