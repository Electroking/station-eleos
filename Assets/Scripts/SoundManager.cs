﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    [SerializeField] private AudioSource audioSource = null;

    [SerializeField] private AudioClip lockedDoor = null;
    [SerializeField] private AudioClip openDoor = null;
    [SerializeField] private AudioClip closeDoor = null;
    // Start is called before the first frame update
    void Start()
    {
        audioSource.clip = openDoor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenDoorSound()
    {
        audioSource.clip = openDoor;
        audioSource.Play();
    }
    public void CloseDoorSound()
    {
        audioSource.clip = closeDoor;
        audioSource.Play();
    }
    public void LockedDoorSound()
    {
        audioSource.clip = lockedDoor;
        audioSource.Play();
    }

}
