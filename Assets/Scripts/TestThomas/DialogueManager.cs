﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{

    public Button nextSentenceButton;
    public Button getOutOfPieceButton;
    public Animator animator;
    public Text nameText;
    public Text dialogueText;
    private  Queue<string> sentences;

    private bool endDialogueMayor;

    private UnityEvent endEvent;

    public bool noAnimation;
    
    // Start is called before the first frame update
    void Start()
    {
        
        sentences = new Queue<string>();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        getOutOfPieceButton.interactable = false;
        animator.SetBool("isOpen", true);

        noAnimation = dialogue.noAnimation;
        endEvent = dialogue.endEvent;
        nameText.text = dialogue.name;
        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();
        StopAllCoroutines();

        StartCoroutine(TYpeSentence(sentence));
    }

    IEnumerator TYpeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(0.02f);
        }
    }

    void EndDialogue()
    {
        
        if (noAnimation)
        {
            animator.SetBool("isOpen", false);
            // nextSentenceButton.enabled = true;
        }
        // else
        // {
        //     nextSentenceButton.enabled = false;
        // }
        getOutOfPieceButton.interactable = true;
        endEvent.Invoke();
    }
    
}
