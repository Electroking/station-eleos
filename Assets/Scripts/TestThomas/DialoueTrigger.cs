﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoueTrigger : MonoBehaviour
{
    public Dialogue dialogue;
    public bool onStart;

    private void Start()
    {
        if (onStart)
        {
            TriggerDialogue();
        }
    }

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }
}
