﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour
{
    public GameObject panelInventoryAdd;

    public AudioSource audioSource;
    public AudioClip notifSound;
    public void DisplayMsg()
    {
        audioSource.clip = notifSound;
        StartCoroutine(DisplayInventoryMsg());
    }
    

    IEnumerator DisplayInventoryMsg()
    {
      panelInventoryAdd.SetActive(true);
      audioSource.Play();
      yield return new WaitForSeconds(3);
      panelInventoryAdd.SetActive(false);
    }
}
